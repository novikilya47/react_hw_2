import React, {Component} from 'react';
import "./styles.css"

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.flag
        }
    }

    render() {
        return (
            <div className = "news_block">
                <h2>{this.props.news.title}</h2>
                <img src ={this.props.news.img}></img>
                <button onClick={this.handleClick}>{this.state.isOpen?"Свернуть":"Развернуть"}</button>
                <p>{this.state.isOpen && this.props.news.info}</p>
            </div>
        )
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.flag !== this.state.isOpen) {
            this.setState({
                isOpen: nextProps.flag
            });
        }
    }

    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}

export default News;