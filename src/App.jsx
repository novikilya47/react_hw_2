import React, {Component} from 'react';
import { news } from './NewsList';
import News from "./NewsClass";
import "./styles.css"

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            news : news,
        }

        this.myRef = React.createRef()
    }

    render() {
        return (
            <><div className ="search">
                <button onClick={this.revertNews}>Перевернуть</button>
                <input type ="text" ref={(input)=>this.myRef=input}></input>
                <button onClick={this.findPost}>Найти</button>
                <button onClick={this.remove}>Сбросить</button>
            </div>
            {this.state.news.map((news, index) => index === 0 ? <News key={news.id} news={news} flag/> : <News key={news.id} news={news}/>)}
            </>
        )
    }

    revertNews = () => {
        this.setState({
            news: this.state.news.reverse()
        })
    }

    findPost = () => {
        this.setState({
            news: this.state.news.filter(post=>post.title.toLowerCase()===this.myRef.value.toLowerCase())
        })
    }

    remove = () =>{
        this.setState({
            news: news
        })
    }
}

export default App;



